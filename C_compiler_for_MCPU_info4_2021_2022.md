# Journal

## January the 17th

Today was mostly about understanding the topic.

We made an attempt to decypher the obfuscated code, and deemed it too long and complicated to be worth the time.
We then investigated different SmallC compilers, most of which were big projects, beyond the scope of ours. 
However we did find a short parser written in OCaml, which could be a useful inspiration.

Figured out the MCPU architecture.

Read and understood the asm.py file.

Started thinking about the implementation of a parser and compiler.

Goal : 
Making a SmallC compiler written in SmallC

Challenge:
The more simplified our implementation of SmallC, the less there is to program, but programming becomes harder because of the limited features. We need to balance the number of features and the code complexity depending on them.

To do :
Read & understand emu.py.

## January the 24th

First draft of the SmallC grammar
We decided not to have any variable types, pointers, or stack. All the variables are global.

Writing the first lines of the parser

## January the 31st

Writing the first macros (writing low level asm programs to build high level programming blocks like loops, ifs, multiplications, etc).

Still working on the parser.

## February the 7th

Still working on the parser.

Still working on the macros.

Definition of grammar.

## February the 14th

Still working on the parser.

Still working on the macros.

Call with M. Richard.

Simple tests on macros with emu.py.

## February the 28th

Mid-term presentation.

## March the 7th

We have made good progress on programming the lexer.
We've discussed token types, organizing what we'll read, and how to implement it.

## March the 14th

We continued to implement the lexer.

Most simple macros work in tests.

## March the 28th

We finished to implement the lexer.

We did a test on a C file and we manage to lex this file correctly.

Now the goal is to implement the parser and make some AST.


